from datetime import datetime

from flask.ext.login import UserMixin
from flask.ext.bcrypt import generate_password_hash, check_password_hash
from peewee import *


DATABASE = SqliteDatabase('social-network.db')


# We can have multiple parents
class User(UserMixin, Model):
    username = CharField(unique=True)
    email = CharField(unique=True)
    password = CharField(max_length=100)
    join_date = DateTimeField(default=datetime.now)
    is_admin = BooleanField(default=False)

    class Meta:
        database = DATABASE
        # - at the start of joined_at means - descending
        order_by = ('-join_date',)

    def get_posts(self):
        return Post.select().where(Post.user == self)

    def get_stream(self):
        return Post.select().where(
            (Post.user << self.following()) |  # << show me the posts of the people I follow 
            (Post.user == self))  # | or mine

    def following(self):
        """ The users that we are following. """
        return (
            User.select().join(
                Relationship, on=Relationship.to_user
            ).where(
                Relationship.from_user == self
            )
        )

    def followers(self):
        """ Get users following the current user """
        return (
            User.select().join(
                Relationship, on=Relationship.from_user
            ).where(
                Relationship.to_user == self
            )
        )

    @classmethod  # let's us use method without creating instance of a class
    def create_user(cls, username, email, password, admin=False): # TODO: What is CLS?
        try:
            # If everything is ok, fine, go ahead. If it fails: rollback everything.
            with DATABASE.transaction():
                cls.create(
                    username=username,
                    email=email,
                    password=generate_password_hash(password),
                    is_admin=admin
                )
        except IntegrityError:
            raise ValueError("User already exists")


class Post(Model):
    timestamp = DateTimeField(default=datetime.now)
    user = ForeignKeyField(
        rel_model=User,
        related_name='posts'
        )
    content = TextField()

    class Meta:
        database = DATABASE
        # , order by multiple fields
        erder_by = ('-timestamp',)

    # ForeignKeyField - A field that points to another database record.


class Relationship(Model):
    from_user = ForeignKeyField(User, related_name='relationships')
    to_user = ForeignKeyField(User, related_name='related_to')

    class Meta:
        database = DATABASE
        indexes = (
            (('from_user', 'to_user'), True)
        )


def initialize():
    DATABASE.connect()
    DATABASE.create_tables([User, Post, Relationship], safe=True)
    DATABASE.close()


# indexes - A list of indexes to create on the model. These could be fields to index for faster searches or,
# as in our case, fields to make unique. More information
# .join() - A query that references another table or another query.
# abort() - Function to immediately end a request with a specified status code.
# errorhandler() - Decorator that marks a function as handling a certain status code.
# return render_template('template.html'), 404 - The 404 on the end specifies the status code for the response.